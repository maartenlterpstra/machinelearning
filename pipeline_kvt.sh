#!/bin/sh
cd code/imagegen

# Make image train/test set for KVT
./genimages.py kvt_train --string_length 30 --imgs_per_font 30
./genimages.py kvt_test --string_length 30 --imgs_per_font 5

cd ../sift

# Cluster KVT train dataset
./cluster.py ../imagegen/kvt_train --means 1500 --tail kvt_1500

# Test KVT classification
./tester.py clusters-kvt_1500.npz ../imagegen/kvt_test