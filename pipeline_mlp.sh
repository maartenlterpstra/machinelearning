#!/bin/sh
cd code/imagegen

# Make image dataset for MLP (data is split in train_tf.py)
./genimages.py mlp_cluster --string_length 30 --imgs_per_font 30 
./genimages.py mlp_train_test --string_length 30 --imgs_per_font 30 

cd ../sift

# Cluster MLP dataset
./cluster.py ../imagegen/mlp_cluster --means 1500 --tail mlp_1500

# Bag features for MLP
./bag.py --tail mlp_1500 ../imagegen/mlp_train_test ./clusters-mlp_1500.npz

# Empty tensorboard directory.
rm -rf log

# Train MLP 
./train_tf.py --tail 1500  bagged_features-mlp_1500.npz clusters-mlp_1500.npz 1000