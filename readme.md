# Fond of typefaces

Machine Learning project of Maarten Terpstra, Folkert de Vries and Han Kruiger at the RUG.

__The contents of this readme are notes that were used by us during the project.
They do not necessarily reflect the current state of the project.__

Pseudo-code for training:
```
initialize matrix S that will contain all features
initialize vector L that will contain the labels

for image in input_images:
	determine SIFT descriptors of image
	add rows with the SIFT descriptor to S
	add font labels to L
```

Pseudo-code for classification:
```
initialize zero array of scores for all fonts.

k_1 = 3
k_2 = 11

for all input features f do:
	determine k_1 nearest clusters
	for the k_2 nearest samples s in receptive fields of k_1 nearest clusters do:
		determine score, based on exponentially weighted squared Euclidean distance
		add score to font that corresponds to the sample s

classify input image as the font with the highest score
get confidence based on 10 highest scores
```

Findings:
Using handpicked fonts (205) a classification rate of 98.06% can be reached using 11-NN
However, this method uses 44 seconds per classification on average
(git commit f90c880550270c7328b1ec460ab16c7d19650971)

Insight:
Pre-process the image. Take the gradient of the image and use that since fonts often 
have clear edges. However, the Laplacian operator is too coarse as it throws out
about 90% of the features. This drastically reduces classification time and correctness
(44% for 1.5 seconds per classification)


# Tried:
LVQ: Quantizing our features does *not* work. This is very likely due to the fact
that our features are not that correlated, i.e. feature A of font X is not that 
similar to feature B of font X. It is much more likely that feature A of font A
is similar to feature A of font Y.

# Dual KNN
To train the data, generate the same huge matrix as before. Once this 
is done, do K-means clustering on the data. When trying to classify a feature,
first classify among the clusters. Select the k_1 closest clusters. Subsequently,
Do KNN classification on the union of the receptive fields of the closest clusters.
This yields the same results as before but yields a tremendous speedup.
(typically somewhere between the 80 and 120x). This is combined with a PCA 
dimensionality reduction.
This is implemented in git commit 653ceb34b03ac3373f9041900f04303482588cf7

# Bag-of-features approach:

* SIFT all input training images. Say number of fonts is n_c
* Determing minimum number of features per font s_min
* Retain only the s_min largest features per font.
* Do k-means clustering on all n_c * s_min features.
* Now we can assign for every input image (= a number of SIFT features) a histogram of the nearest clusters, which is a k-dimensional vector.
