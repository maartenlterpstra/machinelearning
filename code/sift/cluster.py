#!/bin/python2
import numpy as np
import numpy.matlib as npmt
from glob import glob
from random import sample
import argparse
import os
import sklearn.cluster as skcl
import sklearn.decomposition as deco
from joblib import Parallel, delayed


from sifter import Sifter


def sift_and_label_image((i, image_file, num_fonts, num_images)):
    sifter = Sifter()

    # Get font label index from subfolder name
    font_label = int(os.path.split(os.path.dirname(image_file))[1])

    img_descriptors = sifter.get_descriptors(image_file)

    # Make onehot vector for this font
    img_oh_labels = npmt.repmat(np.eye(num_fonts)[font_label], img_descriptors.shape[0], 1)

    # label = [x[0] for x in npmt.repmat(i, img_descriptors.shape[0], 1)]
    img_labels = npmt.array([font_label] * img_descriptors.shape[0])

    print("{0}/{1}".format(i, num_images))
    return (img_descriptors, img_oh_labels, img_labels)

if __name__ == '__main__':
    # Parse arguments
    parser = argparse.ArgumentParser(description='SIFT images of fonts. By default SIFTs all images in the specified folder. By default overwrites data.npz in the current directory.')
    parser.add_argument('source', help='The folder where the images are that need SIFTing')
    parser.add_argument('--means', type=int, default=0, help='Number of means to use in clustering.')
    parser.add_argument('--tail', help='Will be added to the output file names. E.g. \'small\' will result in writing to \'./cluster-small.npz\'.')
    parser.add_argument('--pca_components', type=int, default=64, help='Number of PCA components to use.')

    # Parse arguments
    args = parser.parse_args()
    file_tail = ""
    if not(args.tail is None):
        file_tail = "-" + args.tail

    # Read font folders
    font_folders = glob(args.source + '/*')

    font_names_file = glob(args.source + '/font_names.npz')
    font_names = np.load(font_names_file[0])['font_names']
    num_fonts = len(font_names)

    # Load training image files
    image_files = glob(args.source + '/*/*.png')
    num_images = len(image_files)

    print('Determining SIFT descriptors...')
    result = np.asarray(zip(*Parallel(n_jobs=8)(
        delayed(sift_and_label_image)((i, image_file, num_fonts, num_images))
        for i, image_file in enumerate(image_files)))
    )
    descriptors = np.vstack(np.ravel(result[0]))
    oh_labels = np.vstack(np.ravel(result[1]))
    all_labels = np.hstack(np.ravel(result[2]))

    print('Determined SIFT descriptors, stacking...')
    print('Descriptors shape: {0}, labels shape: {1}'.format(descriptors.shape, oh_labels.shape))
    k = args.means
    if k == 0:
        k = int(len(descriptors) ** 0.5)

    print "Doing incremental PCA..."
    n_components = args.pca_components
    pca = deco.IncrementalPCA(n_components=n_components, batch_size=None, copy=False, whiten=True)
    transformed_data = pca.fit_transform(descriptors)
    print "Done! Variance explained: {0}%".format(round(float(sum(pca.explained_variance_ratio_)) * 100.0))

    print "Doing minibatch k-means clustering over PCA-transformed data with k = {0}...".format(k)
    kmeans_obj = skcl.MiniBatchKMeans(n_clusters=k, n_init=8, verbose=True, max_no_improvement=50, batch_size=10000)  # , init=old_clusters["cluster_centers"])
    kmeans_obj.fit_predict(transformed_data)
    cluster_centers, cluster_labels = kmeans_obj.cluster_centers_, kmeans_obj.labels_
    print "Done with k-means clustering!"

    # cluster_centers is a list of cluster center coordinates in PCA-space
    # cluster_labels is indexes cluster_centers for every vector in transformed_data.

    print "Assigning labels to clusters..."
    cluster_oh_labels = [np.zeros(num_fonts) for i in range(k)]
    for (idx, value) in enumerate(cluster_labels):
        cluster_oh_labels[value] += oh_labels[idx]
    print "Mapping receptive fields..."
    receptive_fields = [[] for i in range(k)]
    for (idx, value) in enumerate(cluster_labels):
        receptive_fields[value].append(idx)

    print('Done! Saving data...')
    np.savez_compressed(
        "clusters" + file_tail + ".npz",
        cluster_centers=cluster_centers,    # k vectors in PCA space that are the means of the clusters
        components=pca.components_,         # The PCA components that were used for the transformation
        comp_mean=pca.mean_,                # The PCA means that were used for the transformation
        var=pca.explained_variance_,        # The variance explained by the PCA (should be > 80%)
        labels=cluster_oh_labels,           # Count of all labels per cluster (WE DON'T USE THIS. REMOVE?)
        font_names=font_names,              # Font names
        receptive_fields=receptive_fields,  # List of lists of indices of original data that belong to clusters
        data=transformed_data,              # All transformed data (receptive_fields indexes this)
        data_labels=all_labels              # All data labels (same indexing as data)
    )

    print('\t... done.')
