#!/bin/python2
# Tensorboard met 'python2 /usr/lib/python2.7/site-packages/tensorflow/tensorboard/tensorboard.py --logdir=/home/[NAAM]/machinelearning/code/sift/log'
# Gebaseerd op
# https://github.com/aymericdamien/TensorFlow-Examples/blob/master/examples/3%20-%20Neural%20Networks/multilayer_perceptron.py

import tensorflow as tf
import numpy as np
import argparse

from sifter import Sifter


# Return a random subset of the data features of size batch_size
def get_batch(data, labels, batch_size):
    # Choose 'batch_size' random indices.
    random_samples = np.random.choice(range(len(data)), batch_size, replace=False)

    # Retrieve those indices.
    xs = data[random_samples, :]
    ys = labels[random_samples, :]

    return (xs, ys)


def partition_data(all_bags, all_labels, train_fraction):
    train_idcs = np.random.choice(range(len(all_bags)), train_fraction * len(all_bags), replace=False)
    test_idcs = [i for i in range(len(all_bags)) if i not in train_idcs]  # Dit kan vast sneller.

    train_bags = all_bags[train_idcs, :]
    train_labels = all_labels[train_idcs, :]
    test_bags = all_bags[test_idcs, :]
    test_labels = all_labels[test_idcs, :]

    return train_bags, train_labels, test_bags, test_labels


def init_perceptron(n_input, hidden_sizes, n_classes):
    # Initialize random weights
    weights, biases = {}, {}
    weights['w1'] = tf.Variable(tf.random_normal([n_input, hidden_sizes[0]]))
    for i in range(1, len(hidden_sizes)):
        weights['w{0}'.format(i + 1)] = tf.Variable(tf.random_normal([hidden_sizes[i - 1], hidden_sizes[i]]))
    weights['w_out'] = tf.Variable(tf.random_normal([hidden_sizes[-1], n_classes]))

    # Initialize random biases for every node in the MLP
    for i in range(len(hidden_sizes)):
        biases['b{0}'.format(i + 1)] = tf.Variable(tf.random_normal([hidden_sizes[i]]))

    biases['b_out'] = tf.Variable(tf.random_normal([n_classes]))
    return (weights, biases)


# Model of the MLP
def multilayer_perceptron(x, weights, biases):
    layers = [tf.nn.sigmoid(tf.add(tf.matmul(x, weights['w1']), biases['b1']))]
    for i in range(len(weights) - 2):
        layers.append(tf.nn.sigmoid(tf.add(tf.matmul(layers[-1], weights['w{0}'.format(i + 2)]), biases['b{0}'.format(i + 2)])))
    return tf.matmul(layers[-1], weights['w_out']) + biases['b_out']

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Train an MLP')
    parser.add_argument('bags', help='The .npz-file that contains the bags.')
    parser.add_argument('clusters', help='The .npz-file that contains the clusters.')
    parser.add_argument('epochs', type=int, help='Number of epochs.')
    parser.add_argument('--tail', help='Will be added to the output file names. E.g. \'foo\' will result in writing to \'./mlp_meta-small.npz\' and \'./mlp_weights-small.tf\'.')
    args = parser.parse_args()

    file_tail = ""
    if not(args.tail is None):
        file_tail = "_" + args.tail

    cluster_data = np.load(args.clusters)
    bags_data = np.load(args.bags)

    cluster_centers = cluster_data['cluster_centers']
    font_names = cluster_data['font_names']

    n_input = len(cluster_centers)
    n_classes = len(font_names)
    hidden_sizes = [25]
    learning_rate = 0.2
    num_epochs = args.epochs
    batch_size = 1200
    num_batches = 50

    all_bags = bags_data['bags']
    all_labels = bags_data['labels']

    train_fraction = 0.9

    train_bags, train_labels, test_bags, test_labels = partition_data(all_bags, all_labels, train_fraction)
    print 'Train bag shape: ', train_bags.shape
    print 'Train labels shape: ', train_labels.shape
    print 'Test bag shape: ', test_bags.shape
    print 'Test labels shape: ', test_labels.shape

    print 'Using {0} classes.'.format(n_classes)

    # Graph input
    x = tf.placeholder("float", [None, n_input])
    # Correct graph output (from the labels)
    y = tf.placeholder("float", [None, n_classes])

    weights, biases = init_perceptron(n_input, hidden_sizes, n_classes)

    # Model of the output as determined by the MLP
    y_mlp = multilayer_perceptron(x, weights, biases)

    # The thing we want to minimize
    # cross_entropy = -tf.reduce_sum(y * tf.log(y_mlp))
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(y_mlp, y))  # Softmax loss
    cost_summary = tf.scalar_summary("cost", cost)

    # Minimize with this optimizer.
    optimizer = tf.train.AdagradOptimizer(learning_rate=learning_rate).minimize(cost)

    # Add summary for TensorBoard
    correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_mlp, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
    accuracy_summary = tf.scalar_summary("accuracy", accuracy)

    init = tf.initialize_all_variables()
    with tf.Session() as session:
        # Define the operation for merging all summaries
        merged_summary_op = tf.merge_all_summaries()

        # Make a summary writer for the train and test summaries
        train_summary_writer = tf.train.SummaryWriter('./log/train', session.graph_def)
        test_summary_writer = tf.train.SummaryWriter('./log/test', session.graph_def)

        # Initialize all variables
        session.run(init)

        train_acc = np.zeros(num_epochs)
        test_acc = np.zeros(num_epochs)
        train_ce = np.zeros(num_epochs)
        test_ce = np.zeros(num_epochs)

        # Training
        for epoch in range(num_epochs):
            # Do num_batches random batches per epoch
            for i in range(num_batches):
                xs, ys = get_batch(train_bags, train_labels, batch_size)
                # Feed the random subset to the optimizer and run it.
                session.run(optimizer, feed_dict={x: xs, y: ys})

            # Get summary string on test and training data for TensorBoard
            summ_str_test = session.run(merged_summary_op, feed_dict={x: test_bags, y: test_labels})
            summ_str_train = session.run(merged_summary_op, feed_dict={x: train_bags, y: train_labels})

            # Write to TensorBoard
            test_summary_writer.add_summary(summ_str_test, epoch)
            train_summary_writer.add_summary(summ_str_train, epoch)

            # Get the current cost/accuracy on the test data and training data,
            # for printing (and plotting).
            train_acc[epoch] = session.run(accuracy, feed_dict={x: train_bags, y: train_labels})
            test_acc[epoch] = session.run(accuracy, feed_dict={x: test_bags, y: test_labels})
            train_ce[epoch] = session.run(cost, feed_dict={x: train_bags, y: train_labels})
            test_ce[epoch] = session.run(cost, feed_dict={x: test_bags, y: test_labels})

            # Print progress
            print "{0}/{1} done.".format((epoch + 1), num_epochs)
            print "\tTest data: \n\t\t Cross entropy: {0}\n\t\t Accuracy: {1}".format(test_ce[epoch], test_acc[epoch])
            print "\tTraining data: \n\t\t Cross entropy: {0}\n\t\t Accuracy: {1}".format(train_ce[epoch], train_acc[epoch])

        print "Optimization finished!"

        saver = tf.train.Saver()
        np.savez_compressed('mlp_meta' + file_tail + '.npz', n_input=n_input, hidden_sizes=hidden_sizes, n_classes=n_classes)
        saver.save(session, 'mlp_weights' + file_tail + '.tf')
        np.savez_compressed('mlp_learning_data.npz', train_acc=train_acc, test_acc=test_acc, train_ce=train_ce, test_ce=test_ce, num_batches=num_batches)
        session.close()
