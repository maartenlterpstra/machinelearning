#!/bin/python2

if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser(description='Plot MLP training data.')
    parser.add_argument('data', help='The .npz-file that contains the accuracies and cross entropies.')
    args = parser.parse_args()

    import numpy as np

    data = np.load(args.data)
    train_acc = data['train_acc']
    test_acc = data['test_acc']
    train_ce = data['train_ce']
    test_ce = data['test_ce']
    num_batches = data['num_batches']
    num_epochs = len(train_acc)

    import matplotlib.pyplot as plt

    fig = plt.figure(figsize=(6, 3))
    plt.plot(num_batches * np.arange(1, num_epochs + 1), 100 * train_acc, 'k:')
    plt.plot(num_batches * np.arange(1, num_epochs + 1), 100 * test_acc, 'k-')
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy (%)')
    plt.tight_layout()
    fig.savefig('mlp_acc.eps')

    fig = plt.figure(figsize=(6, 3))
    plt.plot(num_batches * np.arange(1, num_epochs + 1), train_ce, 'k:')
    plt.plot(num_batches * np.arange(1, num_epochs + 1), test_ce, 'k-')
    plt.xlabel('Epochs')
    plt.ylabel('Cross-entropy')
    plt.tight_layout()
    fig.savefig('mlp_ce.eps')
