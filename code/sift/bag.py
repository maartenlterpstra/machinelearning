#!/bin/python2
import numpy as np
import numpy.matlib as npmt
import argparse
from sifter import Sifter
from glob import glob
from joblib import Parallel, delayed
from test import transform_data
import os


# Return a vector of length 'num_clusters' with counts of how many
# features in 'features' are nearest to that cluster.
def make_bag_of_features(clusters, features):
    num_clusters = len(clusters)
    bag = np.zeros(num_clusters)
    for feature in features:
        # Use squared Euclidean distance for finding the nearest word in our
        # vocabulary for this feature.
        repmatted_feature = npmt.repmat(feature, num_clusters, 1)
        nearest_word = np.argmin(
            np.sum((clusters - repmatted_feature)**2, axis=1), axis=0)
        bag[nearest_word] += 1
    # Normalize bag
    bag = bag / np.linalg.norm(bag)
    return bag


def bag_font_dir((font_dir, cluster_centers, components, comp_mean, var, i, num_fonts)):
    print '{0}/{1}\r'.format(i, num_fonts)
    bags = None
    oh_labels = None
    sifter = Sifter()
    font_id = int(os.path.split(os.path.dirname(font_dir))[1])
    image_files = glob(font_dir + '/*.png')
    # Loop over the individual images of this font.
    for img in image_files:
        # Compute the SIFT descriptors of this image.
        features = sifter.get_descriptors(img)

        # Perform PCA tranformation on this data with the same components etc.
        trans_features = transform_data(features, components, comp_mean, var)

        # Put it in a zakje
        bag = make_bag_of_features(cluster_centers, trans_features)

        # Make one-hot label indicating the font ID.
        img_oh_label = np.eye(num_fonts)[font_id]

        # Add to list of bags/labels
        if bags is None or oh_labels is None:
            bags = bag
            oh_labels = img_oh_label
        else:
            bags = np.vstack((bags, bag))
            oh_labels = np.vstack((oh_labels, img_oh_label))

    return (bags, oh_labels)


if __name__ == '__main__':
    # Parse arguments
    parser = argparse.ArgumentParser(description='Create a bag of features of images in a folder, using the given clusters. The images must be in folders that are named with the images\' labels.')
    parser.add_argument('images', help='The folder where the images are that need preprocessing')
    parser.add_argument('clusters', help='The .npz-file that contains the clusters, labels, and font names.')
    parser.add_argument('--tail', metavar='name', help='Will be added to the output file names. E.g. \'small\' will result in writing to \'./bagged_features-small.npz\'.')
    args = parser.parse_args()

    # Read the necessary data from file
    cluster_data = np.load(args.clusters)
    cluster_centers = cluster_data['cluster_centers']
    pca_components = cluster_data['components']
    pca_mean = cluster_data['comp_mean']
    pca_var = cluster_data['var']
    font_names = cluster_data['font_names']

    sub_dirs = glob(args.images + '/*/')
    num_fonts = len(font_names)

    # Bag the font directories in parallel
    res = zip(*Parallel(n_jobs=8)(
        delayed(bag_font_dir)((font_dir, cluster_centers, pca_components, pca_mean, pca_var, i, num_fonts))
        for i, font_dir in enumerate(sub_dirs))
    )

    file_tail = ""
    if not(args.tail is None):
        file_tail = "-" + args.tail

    # Save bagged features
    np.savez_compressed(
        "bagged_features" + file_tail + ".npz",
        bags=np.vstack((res[0])),   # The bags of features
        labels=np.vstack((res[1]))  # The one-hot labels that indicate the fonts
    )
