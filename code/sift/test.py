#!/bin/python2
import cv2
import numpy as np
import math
import matplotlib.pyplot as plt
import subprocess
import cPickle as pickle
import sys
import os
import timeit
from sklearn.preprocessing import normalize
from joblib import Parallel, delayed
import multiprocessing
from collections import Counter
from scipy.spatial.distance import cdist
import bottleneck
import itertools
from sifter import Sifter
import operator


def replace_right(source, target, replacement, replacements=None):
    return replacement.join(source.rsplit(target, replacements))


# Reward more quality for closer fonts.
def exponential_weight(dist):
    return math.exp(-dist)


def most_common(L):
    # get an iterable of (item, iterable) pairs
    SL = sorted([(label, dist)
                 for i, (label, dist) in enumerate(L)], key=operator.itemgetter(0))

    # Group by font label. This will make a list of groups, where every group
    # has a key (font label), and a list of iterable (label, distance) pairs.
    groups = itertools.groupby(SL, key=operator.itemgetter(0))

    # auxiliary function to get weighted "quality" for a font
    def _quality(group):
        label, matches = group
        quality = 0
        for (_, dist) in matches:
            quality += exponential_weight(dist)
        return quality

    # Return winner's label for this feature
    winner = max(groups, key=_quality)[0]
    return winner


def single_f_dist((feature, data, k_2, labels)):
    def _dist_euclid_sq(data, feature):
        return np.sum((data - feature) ** 2, axis=-1)

    # Return a list of k_2 closest font labels with their distances, in a tuple.
    return map(
        lambda x: (int(labels[x]), _dist_euclid_sq(data[x], feature)),
        bottleneck.argpartsort(
            _dist_euclid_sq(data, feature),
            k_2
        )[:k_2]
    )

num_cores = multiprocessing.cpu_count()
pool = multiprocessing.Pool(num_cores)
print "Of course I'm using all your {0} cores".format(num_cores)


def determine_winner(features, prototypes, receptive_fields, data, data_labels, k_1, k_2):
    print "Searching closest prototypes..."

    distance = cdist(features, prototypes)
    receptive = np.array([None] * distance.shape[0])
    receptive_labels = [None] * distance.shape[0]

    # Gives the k_1 closest prototype indices per input feature
    all_closest_prototypes = np.array([x[:k_1] for x in np.argsort(distance, axis=1)])

    # map that to the appropriate receptive fields
    total = []
    for r in range(features.shape[0]):
        # Fields will contain the indices of features in our data set (which we
        # use for assigning a score for this particular input feature).
        fields = []
        closest_prototypes = all_closest_prototypes[r]
        # Only add receptive field of the k_1 closest prototypes
        for prototype in closest_prototypes:
            fields.extend(receptive_fields[prototype])

        # These are the PCA'd SIFT features of the receptive fields.
        closest_data_features = np.array([data[q, :] for q in fields])
        # These are the labels of the features of the receptive fields.
        closest_labels = np.array([data_labels[q] for q in fields])

        labels = single_f_dist((features[r, :], closest_data_features, k_2, closest_labels))
        total.append(most_common(labels))

    return Counter(total).most_common(10)


def normalize_matrix(a, ax=1):
    row_sums = a.sum(axis=ax)
    if ax == 1:
        return a / row_sums[:, np.newaxis]
    else:
        return a / row_sums[np.newaxis, :]


def normalize(vector):
    return vector / np.sqrt(np.einsum('...i,...i', vector, vector))


def transform_data(X, components, mean, explained_variance):
    return np.dot(X - mean, components.T) / np.sqrt(explained_variance)


def test(inputFile, prototypes, receptive_fields, data, data_labels, components, comp_mean, var, k_1=3, k_2=11):
    start_time = timeit.default_timer()

    print "Reading and SIFTing input image \"{0}\"...".format(inputFile)
    sifter = Sifter()
    descriptors = sifter.get_descriptors(inputFile)
    print "\t... done. Found {0} features".format(descriptors.shape[0])

    # Do PCA transformation
    pca_descriptors = transform_data(descriptors, components, comp_mean, var)

    start_time = timeit.default_timer()

    print "Determining most similar font..."
    winners = determine_winner(pca_descriptors, prototypes, receptive_fields, data, data_labels, k_1, k_2)

    elapsed = timeit.default_timer() - start_time
    print "\t... done. Classification took {0:.3f} seconds".format(elapsed)

    return winners

np.set_printoptions(threshold=np.nan, suppress=True)
if __name__ == '__main__':
    data_all = np.load(sys.argv[2])
    prototypes = data_all['cluster_centers']
    receptive_fields = data_all['receptive_fields']
    data = data_all['data']
    data_labels = data_all['data_labels']
    font_names = data_all['font_names']
    components = data_all['components']
    comp_mean = data_all['comp_mean']
    var = data_all['var']

    winner = test(sys.argv[1], prototypes, receptive_fields,
                  data, data_labels, components, comp_mean, var)
    idxs, votes = zip(*winner)
    print("Classified input as {0} with {2:0.2f}% confidence. (Index {1})".format(
        font_names[idxs[0]], idxs[0], 100.0 * votes[0] / sum(votes)))
    other_names = [font_names[idxs[i]] for i in range(1, len(winner))]
    other_confidences = [
        round(100.0 * votes[i] / sum(votes), 2) for i in range(1, len(winner))]
    strs = ["{0} ({1}%)".format(other_names[i], other_confidences[i])
            for i in range(len(other_names))]
    endStr = replace_right(', '.join(strs), ', ', " or ", 1)
    print("Could also be {0}".format(endStr))
