#!/bin/python2

from classify import classify, init_tf_session
from glob import glob
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--path', help='The input image.', default="../imagegen/realtests")
    parser.add_argument('--clusters', help='The clusters.', default='clusters-1500.npz')
    parser.add_argument('--mlp_meta', help='The MLP metadata.', default='mlp_meta.npz')
    parser.add_argument('--weights', help='The MLP weights.', default='mlp_weights.tf')
    args = parser.parse_args()

    sub_images = glob(args.path + '/*')
    session, classifier, mlp_meta, x, y = init_tf_session(args.mlp_meta, args.weights)
    for image in sub_images:
        font_name = classify(image, args.clusters, session, classifier, mlp_meta, x, y)
        print "Classified {0} as {1}".format(image, font_name)
    session.close()
