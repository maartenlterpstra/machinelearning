#!/bin/python2
# -*- coding: UTF-8 -*-
from test import test
import cv2
import numpy as np
import timeit
from glob import glob
import sys
import os
import argparse


def replace_right(source, target, replacement, replacements=None):
    return replacement.join(source.rsplit(target, replacements))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Test the classification of a batch of images using the KVT approach.')
    parser.add_argument('clusters', help='The file with the clusters AND the receptive fields, PCA components, etc. (Essentially everything.)')
    parser.add_argument('images', help='The folder with the batch of images. Images should be in subdirectories for respective fonts, and the font_names.npz file should be in the root of the folder.')
    parser.add_argument('-k', type=int, default=11, help='Number of closest neighbours to assign a score to.')
    args = parser.parse_args()
    print("Reading data...")
    data_all = np.load(args.clusters)
    prototypes = data_all['cluster_centers']
    receptive_fields = data_all['receptive_fields']
    data = data_all['data']
    data_labels = data_all['data_labels']
    font_names = data_all['font_names']
    components = data_all['components']
    comp_mean = data_all['comp_mean']
    var = data_all['var']

    print('\t... done. Read {0} prototypes of {1} dimensions.'.format(
        prototypes.shape[0], prototypes.shape[1]))

    print "About to classify {0} fonts".format(font_names.shape[0])

    files = glob(args.images + '/*/*.png')
    num_files = len(files)
    score = 0

    start_time_full_loop = timeit.default_timer()
    for (idx, f) in enumerate(files):
        winner = test(f, prototypes, receptive_fields, data, data_labels, components, comp_mean, var)
        idxs, votes = zip(*winner)
        input_font_id = int(os.path.split(os.path.dirname(f))[1])
        confidence = 100.0 * votes[0] / sum(votes)
        error = 0
        for i in range(len(idxs)):
            if input_font_id == idxs[i]:
                break
            else:
                error += 1

        input_font_name = font_names[input_font_id]
        if input_font_id == idxs[0]:
            score += 1
        print "Classified {0} as {1} ({2:0.2f}% confidence)\n".format(input_font_name, font_names[idxs[0]], confidence),
        print "Alternatives: "
        for i, alt in enumerate(idxs[1:]):
            print "\t {0} ({1:0.2f}% confidence)".format(font_names[alt], 100.0 * votes[i + 1] / sum(votes))
        print "Done {0} out of {1} ({2:0.2f}%)".format(idx, num_files, 100.0 * float(idx) / num_files)

    elapsed = timeit.default_timer() - start_time_full_loop
    print "Total number of correct Classifications: {0}".format(score)
    print "Total number of tested fonts:  {0}".format(len(files))
    print "Total time taken: {0}. Time per classification: {1}".format(round(elapsed, 2), round(elapsed / len(files), 3))
    print "Accuracy ≈ {0}%".format(round(100.0 * float(score) / float(len(files)), 1))
