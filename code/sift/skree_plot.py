#!/bin/python2

import numpy as np
import matplotlib.pyplot as plt
import sklearn.decomposition as deco

if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Make skree graph and variance explained per PCA feature graph.')
    parser.add_argument('-data', help='Features that are being PCA\'d.')
    parser.add_argument('-pca_data', help='PCA metadata so we don\'t have to PCA all the time.')
    args = parser.parse_args()

    if args.data is None and args.pca_data is not None:
        meta_data = np.load(args.pca_data)
        explained_variance_ratio = meta_data['explained_variance_ratio']
        explained_variance = meta_data['explained_variance']
    elif args.data is not None and args.pca_data is None:
        descriptors = np.load(args.data)['descriptors']
        pca = deco.IncrementalPCA(n_components=128, batch_size=None, copy=False, whiten=True)
        transformed_data = pca.fit_transform(descriptors)
        explained_variance_ratio = pca.explained_variance_ratio_
        explained_variance = pca.explained_variance_
        np.savez_compressed('pca_metadata.npz', explained_variance=explained_variance, explained_variance_ratio=explained_variance_ratio)
    else:
        print('Nothing to do here.')
        from sys import exit
        exit()

    fig = plt.figure(figsize=(6, 3))
    plt.plot(explained_variance, color='black')
    plt.xlabel('Eigenvectors')
    plt.xlim(0, 128)
    plt.xticks(16 * np.arange(9))
    plt.yticks(4000 * np.arange(5))
    plt.ylabel('Eigenvalue')
    plt.tight_layout()
    fig.savefig('skree.eps')

    fig = plt.figure(figsize=(6, 3))
    plt.plot(100 * np.cumsum(explained_variance_ratio), color='black')
    plt.xlabel('Number of components')
    plt.xlim(0, 128)
    plt.xticks(16 * np.arange(9))
    plt.yticks(20 * np.arange(6))
    plt.ylabel('Explained variance (%)')
    plt.tight_layout()
    fig.savefig('expl_var.eps')
