import cv2
import numpy as np
import sys
import matplotlib.pyplot as plt
from skimage.filters import roberts


class Sifter:

    def __init__(self):
        self.sift = cv2.xfeatures2d.SIFT_create()

    def resize_image(self, img, desired_min_size=500):
        image_height, image_width = img.shape[:2]
        scale = None
        # resize an image in such that the shortest side of the image is
        # 500px
        if image_width < image_height:
            if image_width > desired_min_size:
                scale = float(desired_min_size) / float(image_width)
        else:
            if image_height > desired_min_size:
                scale = float(desired_min_size) / float(image_height)
        # return the original image if resizing is not necessary
        if scale is None:
            return img
        else:
            new_image_width = int(image_width * scale)
            new_image_height = int(image_height * scale)
            return cv2.resize(img, (new_image_width, new_image_height))

    def process_image_alt(self, img, laplacian_threshold=0.4):
        np.set_printoptions(threshold=np.nan)
        dst = None
        dst = cv2.fastNlMeansDenoising(img, dst)
        # Roberts edge detection?
        detected = roberts(dst)
        max_in_image = np.max(detected)
        array_np = np.asarray(detected)
        # 30% filtering seems to work
        low_values_indices = array_np < max_in_image * .3
        array_np[low_values_indices] = 0

        # set remaining values to 1
        array_np = np.asarray(detected)
        low_values_indices = array_np > 0  # Where values are low
        array_np[low_values_indices] = 1  # All low values set to 0

        _, bw = cv2.threshold(
            cv2.convertScaleAbs(detected), 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
        return bw

    def poor_mans_textfilter(self, img):
        _, bw = cv2.threshold(
            cv2.convertScaleAbs(img), 245, 255, cv2.THRESH_BINARY)
        grad = cv2.morphologyEx(bw, cv2.MORPH_OPEN, cv2.getStructuringElement(cv2.MORPH_RECT, (2, 2)))
        return grad

    def canny_textfilter(self, img):
        _, bw = cv2.threshold(
            cv2.convertScaleAbs(img), 250, 255, cv2.THRESH_BINARY)

        canned = cv2.Canny(bw, 0, 255)
        grad = cv2.morphologyEx(bw, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_RECT, (2, 2)))

        mean_val = cv2.mean(grad)
        if mean_val[0] < 128:
            _, bw = cv2.threshold(
                grad, 0, 255, cv2.THRESH_BINARY_INV)
        else:
            _, bw = cv2.threshold(
                grad, 0, 255, cv2.THRESH_BINARY)

        return bw

    def process_image(self, img, laplacian_threshold=75):
        scaled_image = self.resize_image(img)
        dst = None
        dst = cv2.fastNlMeansDenoising(scaled_image, dst)
        morphKernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2, 2))
        grad = cv2.morphologyEx(dst, cv2.MORPH_GRADIENT, morphKernel)
        _, bw = cv2.threshold(
            grad, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
        plt.imshow(bw, cmap='gray')
        plt.show()
        return bw

    def get_descriptors(self, image_file):
        img = cv2.imread(image_file)
        img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

        kp, img_descriptors = self.sift.detectAndCompute(
            self.process_image_alt(img), None)

        # filter descriptors on size
        keypoint_sizes = [ks.size for ks in kp]
        kp_mean = np.mean(keypoint_sizes)
        kp_std = np.std(keypoint_sizes)
        # Throw out features that are too large or too small
        total_idxs = np.where(np.logical_or(
            (keypoint_sizes - kp_mean > 2 * kp_std), (keypoint_sizes - kp_mean < -2 * kp_std)))[0]
        img_descriptors = np.delete(img_descriptors, total_idxs, 0)
        kp = np.delete(kp, total_idxs, 0)

        return img_descriptors

if __name__ == '__main__':
    sifter = Sifter()
    img_path = sys.argv[1]
    img = cv2.imread(img_path)
    img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    plt.imshow(sifter.process_image_alt(img), cmap='gray')
    plt.show()

    processed = sifter.process_image_alt(img)
    plt.imshow(processed, cmap='gray')
    plt.figure()
    sift = cv2.xfeatures2d.SIFT_create()
    kp, img_descriptors = sift.detectAndCompute(
        cv2.convertScaleAbs(processed), None)
    keypoint_sizes = [ks.size for ks in kp]
    kp_mean = np.mean(keypoint_sizes)
    kp_std = np.std(keypoint_sizes)
    # Throw out features that are too large or too small
    total_idxs = np.where(np.logical_or(
        (keypoint_sizes - kp_mean > 2 * kp_std), (keypoint_sizes - kp_mean < -2 * kp_std)))[0]
    img_descriptors = np.delete(img_descriptors, total_idxs, 0)
    kp = np.delete(kp, total_idxs, 0)

    sifted_img = None
    sifted_img = cv2.drawKeypoints(cv2.convertScaleAbs(
        processed), kp, sifted_img, flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    plt.imshow(sifted_img)
    print len(kp)
    plt.show()
