#!/bin/python2

from sifter import Sifter
from bag import make_bag_of_features
from train_tf import multilayer_perceptron, init_perceptron
from test import transform_data
import numpy as np
import tensorflow as tf
import argparse


def init_tf_session(mlp_meta, weights):
    # Load MLP weights
    mlp_meta = np.load(mlp_meta)

    # Graph input
    x = tf.placeholder("float", [None, mlp_meta['n_input']])
    y = tf.placeholder("float", [None, mlp_meta['n_classes']])
    w, b = init_perceptron(
        mlp_meta['n_input'], mlp_meta['hidden_sizes'], mlp_meta['n_classes'])
    y_mlp = multilayer_perceptron(x, w, b)

    saver = tf.train.Saver()
    classifier = tf.argmax(y_mlp, 1)
    session = tf.Session()
    saver.restore(session, weights)
    return session, classifier, mlp_meta, x, y


def classify(input_img, clusters, session, classifier, mlp_meta, x, y):
    # SIFT image
    sifter = Sifter()
    desc = sifter.get_descriptors(input_img)

    # Read clusters
    cluster_data = np.load(clusters)
    components = cluster_data['components']
    comp_mean = cluster_data['comp_mean']
    var = cluster_data['var']

    transformed_features = transform_data(desc, components, comp_mean, var)
    cluster_centers = cluster_data['cluster_centers']
    font_names = cluster_data['font_names']
    num_clusters = len(cluster_centers)

    # Bag image
    bag = make_bag_of_features(cluster_centers, transformed_features)

    # Classify bag
    return font_names[session.run(classifier, feed_dict={x: np.vstack(bag).T, y: np.zeros([1, mlp_meta['n_classes']])})[0]]


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--input_img', help='The input image.', default="../imagegen/realtests/klamme_meem.png")
    parser.add_argument('--clusters', help='The clusters.', default='clusters-1000.npz')
    parser.add_argument('--mlp_meta', help='The MLP metadata.', default='mlp_meta.npz')
    parser.add_argument('--weights', help='The MLP weights.', default='mlp_weights.tf')

    args = parser.parse_args()
    session, classifier, mlp_meta, x, y = init_tf_session(args.mlp_meta, args.weights)
    font_name = classify(args.input_img, args.clusters, session, classifier, mlp_meta, x, y)
    session.close()
    print "Classified image as: {0}".format(font_name)
