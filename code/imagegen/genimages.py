#!/bin/python2
import sys
import os
import subprocess
import string
import random
import argparse
from glob import glob
import numpy as np
import fontconfig as fc
from joblib import Parallel, delayed
from wand.drawing import Drawing
from wand.image import Image, Color
from wand.font import Font
from math import ceil


def get_font_size(text, font_file, pointsize):
    with Image(filename='wizard:') as image:
        with Drawing() as measure:
            measure.font_family = font_file
            measure.font_size = pointsize
            measure.text_antialias = True
            measure.text_kerning = 1

            metrics = measure.get_font_metrics(image, text, multiline=False)
            # Apparently needs some correction factor
            return int(ceil(metrics.text_width)) + 4 * pointsize, int(ceil(metrics.text_height)) + 10


def make_font_image(text, font_file, image_file, pointsize=50):
    width, height = get_font_size(text, font_file, pointsize)
    with Image(width=width, height=height, background=Color('white')) as img:
        with Drawing() as drawing:
            drawing.stroke_color = Color('#000')
            drawing.font = font_file
            drawing.font_size = pointsize
            drawing.gravity = 'center'
            drawing.text_antialias = True
            drawing.text_kerning = 1
            drawing.text(0, 0, text)
            drawing(img)
            img.save(filename=image_file)


def id_generator(size=15, chars=string.ascii_uppercase + string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def make_font_images((i, font_file, image_dir, imgs_per_font, string_length, font_sizes)):
    sub_dir = image_dir + str(i) + '/'
    if not os.path.exists(sub_dir):
        os.makedirs(sub_dir)

    print '{0}/{1}\r'.format(i, len(font_files)),
    sys.stdout.flush()
    for j in range(imgs_per_font):
        # Make random string
        rand_string = id_generator(string_length)
        # Make image
        make_font_image(rand_string, font_file, sub_dir + str(j) + ".png", random.choice(font_sizes))


def generate_labeled_data(font_files, font_names, string_length, imgs_per_font, output_dir, font_sizes=[72]):
    print "Generating labeled dataset. (String length {0}, {1} images per font)".format(string_length, imgs_per_font)

    # Make image directory if it's not there yet.
    image_dir = output_dir + '/'
    if not os.path.exists(image_dir):
        os.makedirs(image_dir)

    # Create the images in parallel
    Parallel(n_jobs=8)(
        delayed(make_font_images)((i, font_file, image_dir, imgs_per_font, string_length, font_sizes))
        for i, font_file in enumerate(font_files)
    )

    # Save font names in .npz file.
    np.savez_compressed(image_dir + "font_names.npz", font_names=font_names)

    # preserve spaces because this guarantees the leftover text in the
    # terminal is overwritten
    print "Done!   "


def load_font_files(directory):
    # Unzip archive if there's no fonts folder.
    if not os.path.exists(directory):
        # Open /dev/null for suppressing stdout when unzipping
        DEVNULL = open(os.devnull, 'w')
        print('Unzipping fonts archive...')
        subprocess.call("tar -xvf fonts.tar.bz2", stdout=DEVNULL, shell=True)
        print('... done')
    # Return all file names in a list that agree with this 'regex'
    return glob('./fonts/*.ttf')

if __name__ == '__main__':
    # Parse arguments
    parser = argparse.ArgumentParser(description='Generate images from .ttf files.')
    parser.add_argument('output_dir', help='Folder where to store the images. This is so we can make a folder with data for the clusters and a folder with data for training/testing')
    parser.add_argument('--string_length', type=int, default=20, help='Length of strings in training images.')
    parser.add_argument('--imgs_per_font', type=int, default=20, help='Number of images per font.')
    args = parser.parse_args()

    # Load font files
    font_files = load_font_files('./fonts')

    # Read English font names from tts files.
    font_names = map(
        lambda x: [str(v) for i, v in fc.FcFont(x).fullname if i == 'en'][0], font_files
    )

    # Sort the font files and the font names in the same order.
    indices = np.argsort(font_names)
    font_files = [font_files[i] for i in indices]
    font_names = [font_names[i] for i in indices]

    # Generate and save data set in the specified output directory.
    generate_labeled_data(font_files, font_names, args.string_length, args.imgs_per_font, args.output_dir)
